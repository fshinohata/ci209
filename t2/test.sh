#!/bin/bash

redis-server ./prof_bin/redis.conf &
SERVER_PID="$!"

sleep 1

./prof_bin/controlador r 100 0 &
CONTROLLER_PID="$!"

./play r &
FOX_PID="$!"

./play g &
GOOSE_PID="$!"

wait ${CONTROLLER_PID}

kill ${FOX_PID}
kill ${GOOSE_PID}
kill ${SERVER_PID}
