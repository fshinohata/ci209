#include <iostream>
#include <stdio.h>
#include <string.h>
#include "utils.hpp"
#include "redis.hpp"
#include "minimax.hpp"
#include "game.hpp"

int main(int argc, char **argv)
{
	GameMaster *master = new GameMaster(argc, argv);
	master->initialize();
	delete master;

	return 0;
}