#ifndef __CPP_REDIS_MANAGER__
#define __CPP_REDIS_MANAGER__

#include <stdlib.h>
#include <hiredis/hiredis.h>
#include "utils.hpp"

class RedisManager
{
private:
	redisContext *context;
	redisReply *reply;
	char *ip;
	int port;
	String *key;
	String *buffer;

public:
	RedisManager(char *_ip, int _port)
	{
		ip = _ip;
		port = _port;
		key = new String;
		buffer = new String;
		context = redisConnect(ip, port);

		if (context == NULL || context->err)
		{
		    if (context)
		    {
		    	fprintf(stderr, "ERROR: Could not connect to redis server (error: %s)\n", context->errstr);
		    }
		    else
		    {
		    	fprintf(stderr, "ERROR: Could not connect to redis server\n");
		    }
		    exit(1);
		}
	}
	
	~RedisManager()
	{
		delete key;
		delete buffer;
	}

	void send(char role, char *message)
	{
		key->setFormatted("jogada_%c", role);
		buffer->set(message);
		reply = (redisReply *)redisCommand(context, "RPUSH %s %s", key->data(), buffer->data());
		/* do something with reply here if necessary */
		freeReplyObject(reply);
	}

	void send(char role, String *message)
	{
		send(role, message->data());
	}

	void receive(char role, char *buf)
	{
		key->setFormatted("tabuleiro_%c", role);
		reply = (redisReply *)redisCommand(context, "BLPOP %s 0", key->data());
		/* do something with reply here if necessary */
		strcpy(buf, reply->element[1]->str);
		freeReplyObject(reply);
	}

	void receive(char role, String *buf)
	{
		key->setFormatted("tabuleiro_%c", role);
		reply = (redisReply *)redisCommand(context, "BLPOP %s 0", key->data());
		/* do something with reply here if necessary */
		buf->set(reply->element[1]->str);
		freeReplyObject(reply);
	}
};

#endif