#ifndef __CPP_UTILS__
#define __CPP_UTILS__

#include <functional>
#include <stdarg.h>
#include <stdlib.h>

#define CPP_UTILS_INITIAL_SIZE 128

class String
{
private:
	int allocated;
	char *ptr;

	void reallocIfNecessary(int newLength)
	{
		if (newLength >= allocated)
		{
			do
			{
				allocated <<= 1;
			}
			while (newLength >= allocated);
			ptr = (char *)realloc(ptr, allocated * sizeof(char));	
		}
	}

public:
	int length;

	String(const char *initial = NULL)
	{
		ptr = (char *)calloc(CPP_UTILS_INITIAL_SIZE, sizeof(char));
		allocated = CPP_UTILS_INITIAL_SIZE;
		length = 0;
		if (initial != NULL)
		{
			int len = strlen(initial);
			append((char *)initial, len);
		}
	}

	~String()
	{
		free(ptr);
	}

	char &operator[](int i)
	{
		return ptr[i];
	}

	inline char &at(int i)
	{
		return ptr[i];
	}

	String *setFormatted(const char *format, ...)
	{
		char aux[1024];
		va_list arguments;
		va_start(arguments, format);
		vsprintf(aux, format, arguments);
		va_end(arguments);
		return set(aux);
	}

	String *set(const char ch)
	{
		char str[2] = { ch, '\0' };
		return set(str);
	}

	String *set(const char *str)
	{
		return set(str, strlen(str));
	}

	String *set(const char *str, int len)
	{
		reallocIfNecessary(len+1);
		strncpy(ptr, str, len+1);
		length = len+1;
		return this;
	}

	String *appendFormatted(const char *format, ...)
	{
		char aux[1024];
		va_list arguments;
		va_start(arguments, format);
		vsprintf(aux, format, arguments);
		va_end(arguments);
		return append(aux);
	}

	String *append(const char ch)
	{
		char str[2] = { ch, '\0' };
		return append(str);
	}

	String *append(const char *str)
	{
		return append(str, strlen(str));
	}

	String *append(const char *str, int len)
	{
		reallocIfNecessary(length + len + 1);
		strncat(ptr, str, len + 1);
		length += len + 1;
		return this;
	}

	inline char *data()
	{
		return ptr;
	}
};

template <class T>
class Array
{
private:
	int allocated;
	T *array;

public:
	int length;
	
	Array<T>()
	{
		array = (T *)malloc(CPP_UTILS_INITIAL_SIZE * sizeof(T));
		length = 0;
		allocated = CPP_UTILS_INITIAL_SIZE;
	}

	~Array<T>()
	{
		free(array);
	}

	Array<T> *push(T elem)
	{
		array[length++] = elem;
		if (length == allocated)
		{
			allocated <<= 1;
			array = (T *)realloc(array, allocated * sizeof(T));
		}
		return this;
	}

	Array<T> *concat(Array<T> arr)
	{
		for (int i = 0; i < arr.length; ++i)
		{
			push(arr[i]);
		}
		return this;
	}

	Array<T> *concat(Array<T> *arr)
	{
		for (int i = 0; i < arr->length; ++i)
		{
			push((*arr)[i]);
		}
		return this;
	}

	Array<T> &operator=(Array<T> arr)
	{
		reset();
		concat(arr);
		return *this;
	}

	Array<T> &operator=(Array<T> *arr)
	{
		reset();
		concat(arr);
		return *this;
	}

	void reset()
	{
		length = 0;
	}

	int indexOf(std::function<bool(const T)> comparer)
	{
		for (int i = 0; i < length; ++i)
		{
			if (comparer(array[i]) == true)
			{
				return i;
			}
		}
		return -1;
	}

	T *where(std::function<bool(const T)> comparer)
	{
		for (int i = 0; i < length; ++i)
		{
			if (comparer(array[i]) == true)
			{
				return &array[i];
			}
		}
		return NULL;
	}

	Array<T> *removeFirst(std::function<bool(const T)> comparer)
	{
		for (int i = 0; i < length; ++i)
		{
			if (comparer(array[i]) == true)
			{
				remove(i);
				break;
			}
		}
		return this;
	}

	Array<T> *remove(int i)
	{
		for (int k = i; k < length-1; ++k)
		{
			array[k] = array[k+1];
		}
		length--;
		return this;
	}

	T &operator[](int i)
	{
		return array[i];
	}

	Array<T> &operator+=(Array<T> arr)
	{
		concat(arr);
		return *this;
	}

	Array<T> &operator+= (Array<T> *arr)
	{
		concat(arr);
		return *this;
	}

	bool includes(std::function<bool(const T)> comparer)
	{
		for (int i = 0; i < length; ++i)
		{
			if (comparer(array[i]) == true)
			{
				return true;
			}
		}
		return false;
	}

	Array<T> *duplicate()
	{
		Array<T> *duplicate = new Array<T>;
		duplicate->concat(this);
		return duplicate;
	}
};

template <class T>
class ListElement
{
public:
	ListElement<T> *previous;
	ListElement<T> *next;
	T *data;
	ListElement(T *_data)
	{
		previous = next = NULL;
		data = _data;
	}
	
	~ListElement()
	{

	}
};

template <class T>
class List
{
private:
	ListElement<T> *head;
	ListElement<T> *tail;

public:
	int length;
	List()
	{
		length = 0;
		head = tail = NULL;
	}

	~List()
	{
		while (length > 0)
		{
			pop();
		}
	}
	
	void push(T *data)
	{
		ListElement<T> *le = new ListElement<T>(data);
		if (length > 0)
		{
			tail->next = le;
			le->previous = tail;
			tail = le;
		}
		else
		{
			head = tail = le;
		}
		length++;
	}

	T *pop()
	{
		ListElement<T> *toRemove = tail;
		T *data = toRemove->data;
		if (length > 1)
		{
			tail = tail->previous;
			tail->next = NULL;
		}
		else
		{
			head = tail = NULL;
		}
		delete toRemove;
		length--;
		return data;
	}

	void unshift(T *data)
	{
		ListElement<T> *le = new ListElement<T>(data);
		if (length > 0)
		{
			head->previous = le;
			le->next = head;
			head = le;
		}
		else
		{
			head = tail = le;
		}
		length++;
	}

	T *shift()
	{
		ListElement<T> *toRemove = head;
		T *data = head->data;
		if (length > 1)
		{
			head = head->next;
			head->previous = NULL;
		}
		else
		{
			head = tail = NULL;
		}
		delete toRemove;
		length--;
		return data;
	}

	void remove(T *data)
	{
		ListElement<T> *runner = head;
		ListElement<T> *toRemove = NULL;
		while (runner != NULL)
		{
			if (runner->data = data)
			{
				toRemove = runner;
				break;
			}
		}
		if (toRemove != NULL)
		{
			if (length > 1)
			{
				if (toRemove == head)
				{
					head = head->next;
					head->previous = NULL;
				}
				else if (toRemove == tail)
				{
					tail = toRemove->previous;
					tail->next = NULL;
				}
				else
				{
					toRemove->next->previous = toRemove->previous;
					toRemove->previous->next = toRemove->next;
				}
			}
			else
			{
				head = tail = NULL;
			}
			length--;
			delete toRemove;
		}
	}

	inline bool isEmpty()
	{
		return length == 0;
	}
};



#endif