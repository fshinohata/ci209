#ifndef __CPP_MINIMAX__
#define __CPP_MINIMAX__

#include <time.h>
#include <limits>
#include "utils.hpp"

template <class StateT, class ActionT>
class MiniMax
{
private:
	int maxDepth;
	int numberOfActionsWhenTimedOut;
	long smallest;
	long biggest;
	int timeout;
	int stopwatch;

public:
	MiniMax()
	{
		numberOfActionsWhenTimedOut = -1;
		timeout = 4;
		maxDepth = 9;
		smallest = std::numeric_limits<long>::min();
		biggest = std::numeric_limits<long>::max();
	}

	virtual ~MiniMax() {}

	inline bool timedOut()
	{
		return (time(NULL) - stopwatch) >= timeout;
	}

	MiniMax *setMaxDepthTo(int _maxDepth)
	{
		maxDepth = _maxDepth;
		return this;
	}

	MiniMax *setTimeoutTo(int _timeout)
	{
		timeout = _timeout;
		return this;
	}

	inline long maximumOf(long a, long b)
	{
		return a > b ? a : b;
	}

	inline long minimumOf(long a, long b)
	{
		return a < b ? a : b;
	}

	long max(StateT s, int currentDepth, long alpha, long beta)
	{
		if (currentDepth == maxDepth || isTerminal(s) || timedOut())
		{
			return score(s);
		}

		Array<ActionT> *actions = getAvailableActionsForMax(s);
		long bestScore = smallest; /* biggest negative */

		for (int i = 0; i < actions->length; ++i)
		{
			StateT newState = apply(s, (*actions)[i]);
			long newScore = min(newState, currentDepth + 1, alpha, beta);
			bestScore = maximumOf(bestScore, newScore);

			if (bestScore >= beta || timedOut())
			{
				deleteState(newState);
				break;
			}

			alpha = maximumOf(alpha, bestScore);
			deleteState(newState);
		}

		deleteActions(actions);
		return bestScore;
	}

	long min(StateT s, int currentDepth, long alpha, long beta)
	{
		if (currentDepth == maxDepth || isTerminal(s) || timedOut())
		{
			return score(s);
		}

		Array<ActionT> *actions = getAvailableActionsForMin(s);
		long bestScore = biggest; /* biggest positive */

		for (int i = 0; i < actions->length; ++i)
		{
			StateT newState = apply(s, (*actions)[i]);
			long newScore = max(newState, currentDepth + 1, alpha, beta);
			bestScore = minimumOf(bestScore, newScore);

			if (bestScore <= alpha || timedOut())
			{
				deleteState(newState);
				break;
			}

			beta = minimumOf(beta, bestScore);
			deleteState(newState);
		}

		deleteActions(actions);
		return bestScore;
	}

	ActionT getBestAction(StateT s)
	{
		stopwatch = time(NULL);
		Array<ActionT> *actions = getAvailableActionsForMax(s);

		if (numberOfActionsWhenTimedOut == -1 || actions->length < numberOfActionsWhenTimedOut)
		{
			maxDepth++;
		}
		
		long bestScore = smallest;
		ActionT *bestAction;

		StateT firstState = apply(s, (*actions)[0]);
		bestScore = max(firstState, 0, smallest, biggest);
		bestAction = &((*actions)[0]);
		deleteState(firstState);
		
		for (int i = 1; i < actions->length; ++i)
		{
			StateT newState = apply(s, (*actions)[i]);
			long newScore = max(newState, 0, smallest, biggest);
			if (newScore >= bestScore)
			{
				bestAction = &((*actions)[i]);
				bestScore = newScore;
			}
			deleteState(newState);
			if (timedOut()) break;
		}

		if (timedOut())
		{
			maxDepth--;
			numberOfActionsWhenTimedOut = actions->length;
		}

		ActionT toSave = createAction();
		copyAction(toSave, *bestAction);
		deleteActions(actions);
		return toSave;
	}

	void deleteActions(Array<ActionT> *actions)
	{
		for (int i = 0; i < actions->length; ++i)
		{
			deleteAction((*actions)[i]);
		}
		delete actions;
	}

	virtual Array<ActionT> *getAvailableActionsForMax(StateT s) = 0;

	virtual Array<ActionT> *getAvailableActionsForMin(StateT s) = 0;

	virtual long score(StateT s) = 0;

	virtual void deleteState(StateT s) = 0;

	virtual ActionT createAction() = 0;

	virtual void deleteAction(ActionT a) = 0;

	virtual void copyAction(ActionT &target, ActionT toCopy) = 0;

	virtual StateT apply(StateT state, ActionT action) = 0;

	virtual bool isTerminal(StateT state) = 0;

	virtual void printAction(ActionT action) = 0;

	virtual void printState(StateT state) = 0;
};

#endif