#ifndef __CPP_GEESE_AND_FOX_GAME__
#define __CPP_GEESE_AND_FOX_GAME__

#include <unistd.h>
#include <limits>
#include <stdlib.h>
#include "utils.hpp"

#define FOX 'r'
#define GOOSE 'g'
#define EMPTY_SPACE '-'
#define NOTHINGNESS ' '
#define WALL '#'
#define MOVE 'm'
#define JUMP 's'
#define NO_MOVE 'n'
#define BOARD_LINES 10
#define BOARD_COLUMNS 10
#define INITIAL_NUMBER_OF_GEESE 13
#define POS(i,j) ((i) * BOARD_COLUMNS + (j))
#define BETWEEN(inf, num, sup) ((inf) <= (num) && (num) <= (sup))
#define ABSOLUTE_VALUE(n) ((n) < 0 ? -(n) : (n))

typedef struct Position Position;

struct Position
{
	int i;
	int j;
};

class GameBoard
{
private:
	String *board;

public:
	Array<Position> *goosePositions;
	Position foxPosition;
	int numberOfGeese;
	bool foxTrapped;

	GameBoard()
	{
		goosePositions = new Array<Position>;
		board = new String;
		foxTrapped = false;
		numberOfGeese = 0;
	}

	~GameBoard()
	{
		delete goosePositions;
		delete board;
	}

	inline char at(int i, int j)
	{
		/*
		 * game board:
		 *   012345678
		 * 0 #########
		 * 1 #  ggg  #
		 * 2 #  ggg  #
		 * 3 #ggggggg#
		 * 4 #-------#
		 * 5 #---r---#
		 * 6 #  ---  #
		 * 7 #  ---  #
		 * 8 #########
		 */
		if (BETWEEN(1, i, 2) || BETWEEN(6, i, 7))
		{
			if (BETWEEN(3, j, 5))
			{
				return (*board)[POS(i, j)];
			}
		}
		else if (BETWEEN(3, i, 5))
		{
			if (BETWEEN(1, j, 7))
			{
				return (*board)[POS(i, j)];
			}
		}
		return NOTHINGNESS;
	}

	inline char at(Position position)
	{
		return at(position.i, position.j);
	}

	void move(Position originalPosition, Position newPosition)
	{
		char character = (*board)[POS(originalPosition.i, originalPosition.j)];
		(*board)[POS(originalPosition.i, originalPosition.j)] = EMPTY_SPACE;
		(*board)[POS(newPosition.i, newPosition.j)] = character;
		
		switch (character)
		{
			case FOX:
				foxPosition = newPosition;
				break;
			case GOOSE:
				Position *toUpdate = goosePositions->where([&, originalPosition](Position p) { return p.i == originalPosition.i && p.j == originalPosition.j; });
				toUpdate->i = newPosition.i;
				toUpdate->j = newPosition.j;
				break;
		}
	}

	void devour(Position goosePosition, Position newFoxPosition)
	{
		(*board)[POS(foxPosition.i, foxPosition.j)] = EMPTY_SPACE;
		(*board)[POS(goosePosition.i, goosePosition.j)] = EMPTY_SPACE;
		goosePositions->removeFirst([&, goosePosition](Position p) { return p.i == goosePosition.i && p.j == goosePosition.j; });
		(*board)[POS(newFoxPosition.i, newFoxPosition.j)] = FOX;
		foxPosition = newFoxPosition;
		numberOfGeese--;
	}

	inline bool isGoose(int i, int j)
	{
		return at(i, j) == GOOSE;
	}

	inline bool isGoose(Position position)
	{
		return isGoose(position.i, position.j);
	}

	inline bool isFox(int i, int j)
	{
		return at(i, j) == FOX;
	}

	inline bool isFox(Position position)
	{
		return isFox(position.i, position.j);
	}

	inline bool isCharacter(int i, int j)
	{
		char element = at(i, j);
		return (element == FOX || element == GOOSE);
	}

	inline bool isCharacter(Position position)
	{
		return isCharacter(position.i, position.j);
	}

	inline bool isObstacle(int i, int j)
	{
		char element = at(i, j);
		return (element == GOOSE || element == WALL || element == NOTHINGNESS);
	}

	inline bool isObstacle(Position position)
	{
		return isObstacle(position.i, position.j);
	}

	inline bool isEmptySpace(int i, int j)
	{
		return at(i, j) == EMPTY_SPACE;
	}

	inline bool isEmptySpace(Position position)
	{
		return at(position) == EMPTY_SPACE;
	}

	inline bool isGooseVulnerable(Position position)
	{
		return (at(position.i-1, position.j  ) == EMPTY_SPACE && at(position.i+1, position.j  ) == EMPTY_SPACE)
			|| (at(position.i  , position.j-1) == EMPTY_SPACE && at(position.i  , position.j+1) == EMPTY_SPACE);
	}

	void updateFoxStatus()
	{
		int i = foxPosition.i;
		int j = foxPosition.j;
		if (
			isObstacle(i-1, j) && isObstacle(i-2, j)
			&& isObstacle(i+1, j) && isObstacle(i+2, j)
			&& isObstacle(i, j-1) && isObstacle(i, j-2)
			&& isObstacle(i, j+1) && isObstacle(i, j+2)
		)
		{
			foxTrapped = true;
		}
		else
		{
			foxTrapped = false;
		}
	}

	GameBoard *parse(const char *str)
	{
		goosePositions->reset();
		board->set(str);
		numberOfGeese = 0;
		foxPosition.i = foxPosition.j = 0;
		for (int i = 1; i < BOARD_LINES; i++)
		{
			for (int j = 1; j < BOARD_COLUMNS; j++)
			{
				if ((*board)[POS(i,j)] == GOOSE)
				{
					numberOfGeese++;
					goosePositions->push({ .i = i, .j = j });
				}
				else if ((*board)[POS(i,j)] == FOX)
				{
					foxPosition.i = i;
					foxPosition.j = j;
					updateFoxStatus();
				}
			}
		}
		return this;
	}

	GameBoard *parse(String *s)
	{
		return parse(s->data());
	}

	GameBoard *duplicate()
	{
		GameBoard *duplicate = new GameBoard;
		duplicate->parse(board);
		return duplicate;
	}

	inline String *toString()
	{
		return board;
	}

	inline char *data()
	{
		return board->data();
	}
};

class Movement
{
public:
	char type; /* n (nop), m (move), s (jump) */
	Position originalPosition;
	Array<Position> *movements;
	Movement()
	{
		movements = new Array<Position>;
	}
	
	Movement(char _type, Position _originalPosition, Position singleMovement)
	{
		type = _type;
		originalPosition = _originalPosition;
		movements = new Array<Position>;
		movements->push(singleMovement);
	}
	
	Movement(char _type, Position _originalPosition, Array<Position> *_movements)
	{
		type = _type;
		originalPosition = _originalPosition;
		movements = new Array<Position>;
		movements->concat(_movements);
	}

	~Movement()
	{
		delete movements;
	}

	Movement &add(Array<Position> *arr)
	{
		movements->concat(arr);
		return *this;
	}

	inline Position first()
	{
		return (*movements)[0];
	}
};

class Player : public MiniMax<GameBoard *, Movement *>
{
private:
	char role;

public:
	Player(char _role)
	{
		role = _role;
		switch (role)
		{
			case FOX:
				setMaxDepthTo(15);
				break;
			case GOOSE:
				setMaxDepthTo(12);
				break;
		}
	}

	~Player()
	{

	}

	GameBoard *apply(GameBoard *board, Movement *movement)
	{
		GameBoard *newBoard = board->duplicate();
		switch (movement->type)
		{
			case MOVE:
				newBoard->move(movement->originalPosition, movement->first());
			break;
			case JUMP:
				for (int i = 0; i < movement->movements->length; i += 2)
				{
					newBoard->devour((*movement->movements)[i], (*movement->movements)[i+1]);
				}
			break;
			default:
				fprintf(stderr, "Movement '%c' not supported.\n", movement->type);
				exit(1);
		}
		return newBoard;
	}

	Array<Movement *> *getAvailableFoxMovements(GameBoard *board)
	{
		Array<Movement *> *movements = new Array<Movement *>;
		Array<Array<Position> *> *jumpPossibilities = new Array<Array<Position> *>;
		Position fp = board->foxPosition;
		
		/* check upwards*/
		if (board->isEmptySpace(fp.i-1, fp.j))
		{
			movements->push(new Movement(MOVE, fp, { .i = fp.i-1, .j = fp.j }));
		}
		else if (board->isGoose(fp.i-1, fp.j) && board->isEmptySpace(fp.i-2, fp.j))
		{
			Array<Position> *first = new Array<Position>;
			first->push({ .i = fp.i-1, .j = fp.j })->push({ .i = fp.i-2, .j = fp.j });
			jumpPossibilities->push(first->duplicate());
			getOtherPossibilities(jumpPossibilities, first, board, { .i = fp.i-2, .j = fp.j });
			delete first;
		}

		/* check rightwards*/
		if (board->isEmptySpace(fp.i, fp.j+1))
		{
			movements->push(new Movement(MOVE, fp, { .i = fp.i, .j = fp.j+1 }));
		}
		else if (board->isGoose(fp.i, fp.j+1) && board->isEmptySpace(fp.i, fp.j+2))
		{
			Array<Position> *first = new Array<Position>;
			first->push({ .i = fp.i, .j = fp.j+1 })->push({ .i = fp.i, .j = fp.j+2 });
			jumpPossibilities->push(first->duplicate());
			getOtherPossibilities(jumpPossibilities, first, board, { .i = fp.i, .j = fp.j+2 });
			delete first;
		}

		/* check downwards */
		if (board->isEmptySpace(fp.i+1, fp.j))
		{
			movements->push(new Movement(MOVE, fp, { .i = fp.i+1, .j = fp.j }));
		}
		else if (board->isGoose(fp.i+1, fp.j) && board->isEmptySpace(fp.i+2, fp.j))
		{
			Array<Position> *first = new Array<Position>;
			first->push({ .i = fp.i+1, .j = fp.j })->push({ .i = fp.i+2, .j = fp.j });
			jumpPossibilities->push(first->duplicate());
			getOtherPossibilities(jumpPossibilities, first, board, { .i = fp.i+2, .j = fp.j });
			delete first;
		}

		/* check leftwards */
		if (board->isEmptySpace(fp.i, fp.j-1))
		{
			movements->push(new Movement(MOVE, fp, { .i = fp.i, .j = fp.j-1 }));
		}
		else if (board->isGoose(fp.i, fp.j-1) && board->isEmptySpace(fp.i, fp.j-2))
		{
			Array<Position> *first = new Array<Position>;
			first->push({ .i = fp.i, .j = fp.j-1 })->push({ .i = fp.i, .j = fp.j-2 });
			jumpPossibilities->push(first->duplicate());
			getOtherPossibilities(jumpPossibilities, first, board, { .i = fp.i, .j = fp.j-2 });
			delete first;
		}

		for (int i = 0; i < jumpPossibilities->length; ++i)
		{
			movements->push(new Movement(JUMP, fp, (*jumpPossibilities)[i]));
			delete (*jumpPossibilities)[i];
		}

		delete jumpPossibilities;

		return movements;
	}

	void getOtherPossibilities(Array<Array<Position> *> *possibilities, Array<Position> *stack, GameBoard *board, Position startPosition)
	{
		/* check upwards*/
		if (board->isGoose(startPosition.i-1, startPosition.j) && board->isEmptySpace(startPosition.i-2, startPosition.j)
			&& !stack->includes([&, startPosition](Position p) { return p.i == startPosition.i-1 && p.j == startPosition.j; }))
		{
			Array<Position> *newStack = stack->duplicate()->push({ .i = startPosition.i-1, .j = startPosition.j })->push({ .i = startPosition.i-2, .j = startPosition.j });
			possibilities->push(newStack->duplicate());
			getOtherPossibilities(possibilities, newStack, board, { .i = startPosition.i-2, .j = startPosition.j });
			delete newStack;
		}

		/* check rightwards */
		if (board->isGoose(startPosition.i, startPosition.j+1) && board->isEmptySpace(startPosition.i, startPosition.j+2)
			&& !stack->includes([&, startPosition](Position p) { return p.i == startPosition.i && p.j == startPosition.j+1; }))
		{
			Array<Position> *newStack = stack->duplicate()->push({ .i = startPosition.i, .j = startPosition.j+1 })->push({ .i = startPosition.i, .j = startPosition.j+2 });
			possibilities->push(newStack->duplicate());
			getOtherPossibilities(possibilities, newStack, board, { .i = startPosition.i, .j = startPosition.j+2 });
			delete newStack;
		}

		/* check downwards */
		if (board->isGoose(startPosition.i+1, startPosition.j) && board->isEmptySpace(startPosition.i+2, startPosition.j)
			&& !stack->includes([&, startPosition](Position p) { return p.i == startPosition.i+1 && p.j == startPosition.j; }))
		{
			Array<Position> *newStack = stack->duplicate()->push({ .i = startPosition.i+1, .j = startPosition.j })->push({ .i = startPosition.i+2, .j = startPosition.j });
			possibilities->push(newStack->duplicate());
			getOtherPossibilities(possibilities, newStack, board, { .i = startPosition.i+2, .j = startPosition.j });
			delete newStack;
		}

		/* check leftwards */
		if (board->isGoose(startPosition.i, startPosition.j-1) && board->isEmptySpace(startPosition.i, startPosition.j-2)
			&& !stack->includes([&, startPosition](Position p) { return p.i == startPosition.i && p.j == startPosition.j-1; }))
		{
			Array<Position> *newStack = stack->duplicate()->push({ .i = startPosition.i, .j = startPosition.j-1 })->push({ .i = startPosition.i, .j = startPosition.j-2 });
			possibilities->push(newStack->duplicate());
			getOtherPossibilities(possibilities, newStack, board, { .i = startPosition.i, .j = startPosition.j-2 });
			delete newStack;
		}
	}

	Array<Movement *> *getAvailableGooseMovements(GameBoard *board)
	{
		Array<Movement *> *movements = new Array<Movement *>;
		for (int i = 0; i < board->goosePositions->length; i++)
		{
			Position g = (*board->goosePositions)[i];
			if (board->isEmptySpace(g.i-1, g.j))
			{
				movements->push(new Movement(MOVE, g, { .i = g.i-1, .j = g.j }));
			}
			if (board->isEmptySpace(g.i+1, g.j))
			{
				movements->push(new Movement(MOVE, g, { .i = g.i+1, .j = g.j }));
			}
			if (board->isEmptySpace(g.i, g.j-1))
			{
				movements->push(new Movement(MOVE, g, { .i = g.i, .j = g.j-1 }));
			}
			if (board->isEmptySpace(g.i, g.j+1))
			{
				movements->push(new Movement(MOVE, g, { .i = g.i, .j = g.j+1 }));
			}
		}
		return movements;
	}

	Array<Movement *> *getAvailableActionsForMax(GameBoard *board)
	{
		switch (role)
		{
			case FOX:
				return getAvailableFoxMovements(board);
			case GOOSE:
				return getAvailableGooseMovements(board);
			default:
				fprintf(stderr, "Not supported.\n");
				exit(1);
		}
	}

	Array<Movement *> *getAvailableActionsForMin(GameBoard *board)
	{
		switch (role)
		{
			case FOX:
				return getAvailableGooseMovements(board);
			case GOOSE:
				return getAvailableFoxMovements(board);
			default:
				fprintf(stderr, "Not supported.\n");
				exit(1);
		}
	}

	inline int distance(Position a, Position b)
	{
		int diff = (a.i - b.i) + (a.j - b.j);
		return ABSOLUTE_VALUE(diff);
	}

	long distanceOfNearestGoose(Position position, GameBoard *board, Array<Position> *usedGeese)
	{
		long minimumDistance = std::numeric_limits<long>::max();
		Position nearestGoose = { .i = 0, .j = 0 };

		for (int i = 0; i < board->goosePositions->length; ++i)
		{
			Position g = (*board->goosePositions)[i];
			if (false == usedGeese->includes([&, g](Position p) { return p.i == g.i && p.j == g.j; }))
			{
				long gooseDistance = distance(position, g);
				if (gooseDistance < minimumDistance)
				{
					minimumDistance = gooseDistance;
					nearestGoose = g;
				}
			}
		}

		usedGeese->push(nearestGoose);
		return minimumDistance;
	}

	long stepsFromTrappingFox(GameBoard *board)
	{
		/* north-north, north, south-south, south, ... */
		int nn, n, ss, s, ee, e, ww, w;
		nn = n = ss = s = ee = e = ww = w = std::numeric_limits<int>::max();
		Array<Position> *usedGeese = new Array<Position>;
		Position fp = board->foxPosition;

		if (board->isObstacle(fp.i-2, fp.j  )) nn = 0; else nn = distanceOfNearestGoose({ .i = fp.i-2, fp.j   }, board, usedGeese);
		if (board->isObstacle(fp.i-1, fp.j  )) n  = 0; else n  = distanceOfNearestGoose({ .i = fp.i-1, fp.j   }, board, usedGeese);
		if (board->isObstacle(fp.i  , fp.j+2)) ee = 0; else ee = distanceOfNearestGoose({ .i = fp.i  , fp.j+2 }, board, usedGeese);
		if (board->isObstacle(fp.i  , fp.j+1)) e  = 0; else e  = distanceOfNearestGoose({ .i = fp.i  , fp.j+1 }, board, usedGeese);
		if (board->isObstacle(fp.i+2, fp.j  )) ss = 0; else ss = distanceOfNearestGoose({ .i = fp.i+2, fp.j   }, board, usedGeese);
		if (board->isObstacle(fp.i+1, fp.j  )) s  = 0; else s  = distanceOfNearestGoose({ .i = fp.i+1, fp.j   }, board, usedGeese);
		if (board->isObstacle(fp.i  , fp.j-2)) ww = 0; else ww = distanceOfNearestGoose({ .i = fp.i  , fp.j-2 }, board, usedGeese);
		if (board->isObstacle(fp.i  , fp.j-1)) w  = 0; else w  = distanceOfNearestGoose({ .i = fp.i  , fp.j-1 }, board, usedGeese);

		delete usedGeese;
		return nn + n + e + ee + s + ss + w + ww;
	}

	long geeseThatCanMove(GameBoard *board)
	{
		long geeseThatCanMove = 0;
		for (int i = 0; i < board->goosePositions->length; ++i)
		{
			Position g = (*board->goosePositions)[i];
			if (board->isEmptySpace(g.i-1, g.j) || board->isEmptySpace(g.i+1, g.j) || board->isEmptySpace(g.i, g.j-1) || board->isEmptySpace(g.i, g.j+1))
			{
				geeseThatCanMove++;
			}
		}
		return geeseThatCanMove;
	}

	long foxScore(GameBoard *board)
	{
		if (board->numberOfGeese < 4) return 999999L;
		if (board->foxTrapped) return 0L;
		long n1 = 1000L * board->numberOfGeese;
		long n2 = 100L * stepsFromTrappingFox(board);
		return (13000L - n1) + n2;
	}

	long geeseScore(GameBoard *board)
	{
		if (board->foxTrapped) return 999999L;
		if (board->numberOfGeese < 4) return 0L;
		long n1 = 8000L * board->numberOfGeese;
		long n2 = 350L * stepsFromTrappingFox(board);
		long n4 = 100L * geeseThatCanMove(board);
		return (n4 + n1) - n2;
	}

	long score(GameBoard *board)
	{
		switch (role)
		{
			case FOX:
				return foxScore(board);
			case GOOSE:
				return geeseScore(board);
			default:
				fprintf(stderr, "Not supported.\n");
				exit(1);
		}
	}

	inline bool isTerminal(GameBoard *board)
	{
		return board->foxTrapped == true || board->numberOfGeese < 4;
	}

	void deleteState(GameBoard *board)
	{
		delete board;
	}

	void copyAction(Movement *&target, Movement *source)
	{
		target->type = source->type;
		target->originalPosition = source->originalPosition;
		target->movements->concat(source->movements);
	}

	Movement *createAction()
	{
		return new Movement();
	}

	void deleteAction(Movement *movement)
	{
		delete movement;
	}

	void printAction(Movement *movement)
	{
		printf("Movement {\n\ttype: %c\n\toriginalPosition: { i: %d, j: %d }\n\tmovements: {\n", movement->type, movement->originalPosition.i, movement->originalPosition.j);
		for (int i = 0; i < movement->movements->length; i++)
		{
			printf("\t\t{ i: %d, j: %d }\n", (*movement->movements)[i].i, (*movement->movements)[i].j);
		}
		printf("\t}\n}\n");
	}

	void printState(GameBoard *board)
	{
		if (board) {}
	}
};

class GameMaster
{
private:
	RedisManager *redis;
	String *tempBuffer;
	Player *player;
	int timeout;

public:
	char role;
	GameBoard *board;

	GameMaster(int argc, char **argv)
	{
		board = new GameBoard;
		tempBuffer = new String;

		if (argc < 2)
		{
			printf("Usage: ./play role [timeout] [ip] [port]\n");
			exit(1);
		}

		role = argv[1][0];

		if (role != FOX && role != GOOSE)
		{
			printf("The role must be either 'r' or 'g'!");
			exit(1);
		}


		int timeout = argc > 2 ? atoi(argv[2]) : 0;
		char *ip = argc > 3 ? argv[3] : (char *)"127.0.0.1";
		int port = argc > 4 ? atoi(argv[4]) : 10001;

		player = new Player(role);

		if (timeout > 0)
		{
			player->setTimeoutTo(timeout);
		}

		redis = new RedisManager(ip, port);
	}

	~GameMaster()
	{
		delete redis;
		delete board;
		delete tempBuffer;
		delete player;
	}

	void receiveBoard()
	{
		redis->receive(role, tempBuffer);
		board->parse(retrieveControllerBoard(tempBuffer));
	}

	void send(String *message)
	{
		redis->send(role, message);
	}

	void initialize()
	{
		if (player == NULL)
		{
			fprintf(stderr, "ERROR: No player specified before call of GameMaster::initialize()\n");
			exit(1);
		}
		receiveBoard();
		play();
	}

	bool won()
	{
		switch (role)
		{
			case FOX:
				return board->numberOfGeese < 4;
			case GOOSE:
				return board->foxTrapped;
			default:
				fprintf(stderr, "ERROR: Invalid role '%c' used in function GameMaster::won()\n", role);
				exit(1);
		}
	}

	bool lost()
	{
		switch (role)
		{
			case FOX:
				return board->foxTrapped;
			case GOOSE:
				return board->numberOfGeese < 4;
			default:
				fprintf(stderr, "ERROR: Invalid role '%c' used in function GameMaster::won()\n", role);
				exit(1);
		}
	}

	void play()
	{
		while (!won() && !lost())
		{
			Movement *nextMove = player->getBestAction(board);
			String *translatedMove = translateMovementToControlSequence(nextMove);

			send(translatedMove);
			
			delete nextMove;
			delete translatedMove;

			// sleep(10);

			receiveBoard();

			// String *nextMove = player->getNextMove(board);
			// send(nextMove);
			// delete nextMove;
			// receiveBoard();
		}
	}

	String *translateMovementToControlSequence(Movement *m)
	{
		String *translated = new String;
		translated->appendFormatted("%c %c ", role, m->type);
		
		switch (m->type)
		{
			case MOVE:
				translated->appendFormatted("%d %d %d %d", m->originalPosition.i, m->originalPosition.j, m->first().i, m->first().j);
				break;
			case JUMP:
				translated->appendFormatted("%d %d %d ", (m->movements->length / 2) + 1, m->originalPosition.i, m->originalPosition.j);
				for (int i = 0; i < m->movements->length; i += 2)
				{
					/* 0, 2, 4, ... are goose positions, 1, 3, 5, ... are fox positions */
					translated->appendFormatted("%d %d ", (*m->movements)[i+1].i, (*m->movements)[i+1].j);
				}
				break;
			default:
				fprintf(stderr, "Movement type '%c' is not supported.\n", m->type);
				exit(1);
		}

		return translated;
	}

	char *retrieveControllerBoard(String *responseBuffer)
	{
		/*
		 * controller response format:
		 * <role>\n
		 * <role> n\n
		 * <board>
		 * i.e. needs to jump two lines to get the board
		 */
		int linesJumped = 0;
		int i;
		for (i = 0; i < responseBuffer->length; i++)
		{
			if ((*responseBuffer)[i] == '\n')
			{
				linesJumped++;
				if (linesJumped == 2)
				{
					i++;
					break;
				}
			}
		}

		return responseBuffer->data() + i;
	}
};

#endif