#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "collections.h"

#define MINIMUM_ALLOCATION 32

void freeListElement(ListElement *le)
{
    free(le);
}

ListElement *createListElement(void *data)
{
    ListElement *le = (ListElement *)malloc(sizeof(ListElement));
    le->previous = le->next = NULL;
    le->data = data;
    return le;
}

ListElement *listPush(List **lp, void *data)
{
    List *l = *lp;
    ListElement *le = createListElement(data);
    if (listSize(l) > 0)
    {
        l->tail->next = le;
        le->previous = l->tail;
        l->tail = le;
    }
    else
    {
        l->head = l->tail = le;
    }
    (l->size)++;
    return le;
}

void *listPop(List **lp)
{
    List *l = *lp;
    ListElement *toRemove = l->tail;
    void *data = toRemove->data;
    if (listSize(l) > 1)
    {
        l->tail = l->tail->previous;
        l->tail->next = NULL;
    }
    else
    {
        l->tail = l->head = NULL;
    }
    (l->size)--;
    freeListElement(toRemove);
    return data;
}

ListElement *listUnshift(List **lp, void *data)
{
    List *l = *lp;
    ListElement *le = createListElement(data);
    if (listSize(l) > 0)
    {
        l->head->previous = le;
        le->next = l->head;
        l->head = le;
    }
    else
    {
        l->head = l->tail = le;
    }
    (l->size)++;
    return le;
}

void *listShift(List **lp)
{
    List *l = *lp;
    ListElement *toRemove = l->head;
    void *data = toRemove->data;
    if (listSize(l) > 1)
    {
        l->head = l->head->next;
        l->head->previous = NULL;
    }
    else
    {
        l->head = l->tail = NULL;
    }
    (l->size)--;
    freeListElement(toRemove);
    return data;
}

void *listRemove(List **lp, ListElement *le)
{
	List *l = *lp;
	void *data = le->data;
	if (le != l->head && le != l->tail)
	{
		le->next->previous = le->previous;
		le->previous->next = le->next;
	}
	else if (le == l->head)
	{
		le->next->previous = NULL;
	}
	else
	{
		le->previous->next = NULL;
	}
	free(le);
	(l->size)--;
	return data;
}

void freeList(List *l, void (*freeData)(void *data))
{
	if (freeData != NULL)
	{
		while (listSize(l) > 0)
		{
			freeData(l->shift(&l));
		}
	}
	else
	{
		while (listSize(l) > 0)
		{
			l->shift(&l);
		}
	}
	free(l);
}

List *createList()
{
    List *l = (List *)malloc(sizeof(List));
    l->head = l->tail = NULL;
	l->push = listPush;
	l->pop = listPop;
	l->shift = listShift;
	l->unshift = listUnshift;
	l->remove = listRemove;
	l->size = 0;
    return l;
}

void freePriorityQueueElement(PriorityQueueElement *pqe)
{
	free(pqe);
}

PriorityQueueElement *createPriorityQueueElement(void *data, long priority)
{
	PriorityQueueElement *pqe = (PriorityQueueElement *)malloc(sizeof(PriorityQueueElement));
	pqe->next = pqe->previous = NULL;
	pqe->data = data;
	pqe->priority = priority;
	return pqe;
}

bool orderQueueAscending(PriorityQueueElement *a, PriorityQueueElement *b)
{
	return a->priority <= b->priority;
}

bool orderQueueDescending(PriorityQueueElement *a, PriorityQueueElement *b)
{
	return a->priority >= b->priority;
}

PriorityQueueElement *priorityQueueEnqueue(PriorityQueue **qp, void *data, long priority)
{
	PriorityQueue *q = *qp;
	PriorityQueueElement *new = createPriorityQueueElement(data, priority);
	if (queueSize(q) > 0)
	{
		PriorityQueueElement *r = q->head;
		while (r != NULL && q->order(r, new))
		{
			r = r->next;
		}
		if (r != q->head && r != NULL)
		{
			new->next = r;
			new->previous = r->previous;
			r->previous->next = new;
			r->previous = new;
		}
		else if (r == q->head)
		{
			r->previous = new;
			new->next = r;
			q->head = new;
		}
		else if (r == NULL)
		{
			q->tail->next = new;
			new->previous = q->tail;
			q->tail = new;
		}
	}
	else
	{
		q->head = q->tail = new;
	}
	(q->size)++;
	return new;
}

void *priorityQueueDequeue(PriorityQueue **qp)
{
	PriorityQueue *q = *qp;
	void *data = NULL;
	if (queueSize(q) > 0)
	{
		data = q->head->data;
		PriorityQueueElement *toRemove = q->head;
		q->head = q->head->next;
		if (q->head != NULL)
		{
			q->head->previous = NULL;
		}
		else
		{
			q->tail = NULL;
		}
		freePriorityQueueElement(toRemove);
		(q->size)--;
	}
	return data;
}

void *priorityQueueRemove(PriorityQueue **qp, PriorityQueueElement *qe)
{
	PriorityQueue *q = *qp;
	void *data = qe->data;
	if (qe != q->head && qe != q->tail)
	{
		qe->next->previous = qe->previous;
		qe->previous->next = qe->next;
	}
	else if (qe == q->head)
	{
		qe->next->previous = NULL;
	}
	else
	{
		qe->previous->next = NULL;
	}
	free(qe);
	(q->size)--;
	return data;
}

void freePriorityQueue(PriorityQueue *pq, void (*freeData)(void *data))
{
	if (freeData != NULL)
	{
		while (queueSize(pq) > 0)
		{
			freeData(pq->dequeue(&pq));
		}
	}
	else
	{
		while (queueSize(pq) > 0)
		{
			pq->dequeue(&pq);
		}
	}
	free(pq);
}

PriorityQueue *createPriorityQueue(bool orderAscending)
{
	PriorityQueue *pq = (PriorityQueue *)malloc(sizeof(PriorityQueue));
	pq->head = pq->tail = NULL;
	pq->size = 0;
	pq->enqueue = priorityQueueEnqueue;
	pq->dequeue = priorityQueueDequeue;
	pq->remove = priorityQueueRemove;
	if (orderAscending)
	{
		pq->order = orderQueueAscending;
	}
	else
	{
		pq->order = orderQueueDescending;
	}
	return pq;
}

void freeQueueElement(QueueElement *qe)
{
	free(qe);
}

QueueElement *createQueueElement(void *data)
{
	QueueElement *qe = (QueueElement *)malloc(sizeof(QueueElement));
	qe->previous = qe->next = NULL;
	qe->data = data;
	return qe;
}

QueueElement *queueEnqueue(Queue **qp, void *data)
{
	Queue *q = *qp;
	QueueElement *qe = createQueueElement(data);
	if (queueSize(q) > 0)
	{
		if (q->head->next != NULL)
		{
			q->tail->next = qe;
		}
		else
		{
			q->head->next = q->tail->next = qe;
		}
		q->tail->next->previous = q->tail;
		q->tail = q->tail->next;
	}
	else
	{
		q->head = q->tail = qe;
	}
	(q->size)++;
	return qe;
}

void *queueDequeue(Queue **qp)
{
	Queue *q = *qp;
	if (queueSize(q) > 0)
	{
		void *data = q->head->data;
		QueueElement *toRemove = q->head;
		q->head = q->head->next;
		if (q->head != NULL)
		{
			q->head->previous = NULL;
		}
		else
		{
			q->tail = NULL;
		}
		freeQueueElement(toRemove);
		(q->size)--;
		return data;
	}
	return NULL;
}

void *queueRemove(Queue **qp, QueueElement *qe)
{
	Queue *q = *qp;
	void *data = qe->data;
	if (qe != q->head && qe != q->tail)
	{
		qe->next->previous = qe->previous;
		qe->previous->next = qe->next;
	}
	else if (qe == q->head)
	{
		qe->next->previous = NULL;
	}
	else
	{
		qe->previous->next = NULL;
	}
	free(qe);
	(q->size)--;
	return data;
}

void freeQueue(Queue *q, void (*freeData)(void *data))
{
	if (freeData != NULL)
	{
		while (queueSize(q) > 0)
		{
			freeData(q->dequeue(&q));
		}
	}
	else
	{
		while (queueSize(q) > 0)
		{
			q->dequeue(&q);
		}
	}
	free(q);
}

Queue *createQueue()
{
	Queue *q = (Queue *)malloc(sizeof(Queue));
	q->head = q->tail = NULL;
	q->size = 0;
	q->enqueue = queueEnqueue;
	q->dequeue = queueDequeue;
	return q;
}

void freeArrayElement(ArrayElement *ae)
{
    free(ae);
}

ArrayElement *createArrayElement(void *data)
{
    ArrayElement *ae = (ArrayElement *)malloc(sizeof(ArrayElement));
    ae->data = data;
    return ae;
}

void *arrayPop(Array **ap)
{
    Array *a = *ap;
    (a->arrayLength)--;
    void *data = a->array[a->arrayLength]->data;
    freeArrayElement(a->array[a->arrayLength]);
    return data;
}

ArrayElement *arrayPush(Array **ap, void *data)
{
    Array *a = *ap;
    ArrayElement *ae = createArrayElement(data);
    a->array[a->arrayLength] = ae;
    (a->arrayLength)++;
    if (a->arrayLength == a->arrayAllocated)
    {
        a->arrayAllocated <<= 1;
        a->array = (ArrayElement **)realloc(a->array, a->arrayAllocated * sizeof(ArrayElement *));
    }
    return ae;
}

void *arrayGet(Array *a, long index)
{
    return a->array[index]->data;
}

void *arrayRemoveAt(Array **ap, long index)
{
	Array *a = *ap;
	ArrayElement *toRemove = a->array[index];
	void *data = toRemove->data;
	for (long i = index; i < arraySize(a) - 1; i++)
	{
		a->array[i] = a->array[i + 1];
	}
	(a->arrayLength)--;
	freeArrayElement(toRemove);
	return data;
}

void freeArray(Array *a)
{
    free(a->array);
    free(a);
}

Array *createArray()
{
    Array *a = (Array *)malloc(sizeof(Array));
    a->array = (ArrayElement **)malloc(MINIMUM_ALLOCATION * sizeof(ArrayElement *));
    a->arrayLength = 0;
    a->arrayAllocated = MINIMUM_ALLOCATION;
    a->push = arrayPush;
    a->pop = arrayPop;
    a->get = arrayGet;
    a->removeAt = arrayRemoveAt;
    return a;
}

void printQueue(Queue *q, const char *(*printQueueElementData)(void *data))
{
	if (queueSize(q) > 0)
	{
		printf("Queue: {");
		printf(" \"%s\"", printQueueElementData(q->head->data));

		QueueElement *runner = q->head->next;
		
		while (runner != NULL)
		{
			printf(", \"%s\"", printQueueElementData(runner->data));
			runner = runner->next;
		}

		printf(" }\n");
	}
	else
	{
		printf("Queue empty!\n");
	}
}

void printPriorityQueue(PriorityQueue *q)
{
	if (queueSize(q) > 0)
	{
		printf("\
{\n\
	\"PriorityQueue\": <%p>,\n\
	\"size\": %ld,\n\
	\"head\": <%p>,\n\
	\"tail\": <%p>,\n\
	\"elements\": {\n", q, q->size, q->head, q->tail);

		printf("\
		{ \"address\": <%p>, \"priority\": %ld, \"previous\": <%p>, \"next\": <%p>, \"data\": <%p> }", q->head, q->head->priority, q->head->previous, q->head->next, q->head->data);

		PriorityQueueElement *runner = q->head->next;
		
		while (runner != NULL)
		{
			printf(",\n\
		{ \"address\": <%p>, \"priority\": %ld, \"previous\": <%p>, \"next\": <%p>, \"data\": <%p> }", runner, runner->priority, runner->previous, runner->next, runner->data);
			runner = runner->next;
		}

		printf("\
	}\n");
		printf("}\n");
	}
	else
	{
		printf("Queue empty!\n");
	}
}
