#include <stdio.h>
#include <stdlib.h>
#include "collections.h"
#include "stateTree.h"

#define MINIMUM_ALLOCATION 32
#define max(a, b) ((a) > (b) ? (a) : (b))

void updateStateNodeHeight(StateNode *sn)
{
	long maxHeight = 0;
	for (long i = 0; i < sn->nextStatesLength; i++)
	{
		maxHeight = max(maxHeight, sn->nextStates[i]->height + 1);
	}
	sn->height = maxHeight;
}

void updateStateNodeAndAncestorsHeight(StateNode *sn)
{
	updateStateNodeHeight(sn);
	while (sn->parent != NULL)
	{
		updateStateNodeHeight(sn->parent);
		sn = sn->parent;
	}
}

long treeDepth(StateTree *tree)
{
	return tree->root->height;
}

void addNextState(StateNode *parent, void *state, void *actionApplied)
{
	StateNode *sn = createStateNode(parent->tree, parent);
	sn->actionToReach = actionApplied;
	sn->state = state;
	sn->heuristicScore = parent->tree->heuristic(state);
	parent->nextStates[parent->nextStatesLength] = sn;
	updateStateNodeAndAncestorsHeight(sn);
	(parent->nextStatesLength)++;
	if (parent->nextStatesLength == parent->nextStatesAllocated)
	{
		parent->nextStatesAllocated <<= 1;
		parent->nextStates = (StateNode **)realloc(parent->nextStates, parent->nextStatesAllocated * sizeof(StateNode *));
	}
}

void freeStateNodeRecursively(StateNode *sn)
{
	for (long i = 0; i < sn->nextStatesLength; i++)
	{
		freeStateNodeRecursively(sn->nextStates[i]);
	}
	freeStateNode(sn);
}

void freeStateNode(StateNode *sn)
{
	free(sn->nextStates);
	sn->tree->freeAction(sn->actionToReach);
	sn->tree->freeState(sn->state);
	free(sn);
}

StateNode *createStateNode(StateTree *tree, StateNode *parent)
{
	StateNode *sn = (StateNode *)malloc(sizeof(StateNode));
	sn->nextStates = (StateNode **)malloc(MINIMUM_ALLOCATION * sizeof(StateNode *));
	sn->nextStatesLength = 0;
	sn->nextStatesAllocated = MINIMUM_ALLOCATION;
	sn->parent = parent;
	sn->tree = tree;
	sn->actionToReach = NULL;
	sn->heuristicScore = -1;
	sn->height = 0;
	sn->state = NULL;
	return sn;
}

void freeStateTree(StateTree *st)
{
	freeStateNodeRecursively(st->root);
	free(st);
}

StateTree *createStateTree(
	void (*freeAction)(void *action), 
	void (*freeState)(void *state),
	long (*heuristic)(void *state))
{
	StateTree *st = (StateTree *)malloc(sizeof(StateTree));
	st->root = createStateNode(st, NULL);
	st->freeAction = freeAction;
	st->freeState = freeState;
	st->heuristic = heuristic;
	return st;
}

void printStateNode(StateNode *sn)
{
	printf("StateNode: {\n\taddress: <%p>,\n\tparent: <%p>,\n\ttree: <%p>,\n\tstate: <%p>,\n\tactionToReach: <%p>,\n\theuristicScore: %ld,\n\theight: %ld,\n\t", sn, sn->parent, sn->tree, sn->state, sn->actionToReach, sn->heuristicScore, sn->height);
	printf("nextStates[%ld]: {\n", sn->nextStatesLength);
	for (long i = 0; i < sn->nextStatesLength; i++)
	{
		printf("\t\t{ address: <%p>, parent: <%p>, actionToReach: <%p>, heuristicScore: %ld }\n", sn->nextStates[i], sn->nextStates[i]->parent, sn->nextStates[i]->actionToReach, sn->nextStates[i]->heuristicScore);
	}
	printf("\t}\n}\n");
}

void printStateTree(StateTree *st)
{
	printf("StateTree: {\n\taddress: <%p>,\n\troot: <%p>\n}\n", st, st->root);
	printStateNode(st->root);
}