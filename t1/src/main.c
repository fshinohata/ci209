#include <stdio.h>
#include "collections.h"
#include "aStar.h"
#include "floodIt.h"

#define useResult(a) if (a) {}

int main(void)
{
    long n, m, colors;

    useResult(scanf("%ld %ld %ld", &n, &m, &colors));
    
    FloodItBoard *b = createFloodItBoard(n, m, colors);
    
    readBoardFromStdin(b);

    FloodItGraph *t = generateFloodItGraph(b);

    freeFloodItBoard(b);

    long maxDepth = 1;
    List *plan = aStar((void *)t, maxDepth, freeAction, freeState, /*evaluate:*/score, expand, isTerminal, /*isEvaluateHeuristic:*/false);
    // List *plan = aStar((void *)t, maxDepth, freeAction, freeState, /*evaluate:*/heuristic, expand, isTerminal, /*isEvaluateHeuristic:*/true);

    printf("%ld\n", listSize(plan));
    while (listSize(plan) > 0)
    {
        printf("%ld ", (long)plan->shift(&plan));
    }
    printf("\n");

    freeList(plan, NULL);
    return 0;
}