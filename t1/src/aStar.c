#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "collections.h"
#include "stateTree.h"
#include "aStar.h"

List *aStar(
	void *initialState,
	long maxDepth,
	void (*freeAction)(void *action),
	void (*freeState)(void *state),
	long (*evaluate)(void *state),
	void (*expand)(StateNode *sn),
	bool (*isTerminal)(StateNode *sn),
	bool evaluateIsHeuristic)
{
	List *steps = createList();
	/* is evaluate is a score function, the priority queue will be ordered descending (highest score => the better) */
	PriorityQueue *openNodes = createPriorityQueue(/*orderAscending:*/evaluateIsHeuristic);
	AStarTree *ast = createAStarTree(initialState, maxDepth >= 0 ? maxDepth : 0, freeAction, freeState, evaluate);
	StateNode *root = ast->tree->root;

	openNodes->enqueue(&openNodes, (void *)root, root->heuristicScore);

	while (queueSize(openNodes) > 0)
	{
		StateNode *n = (StateNode *)openNodes->dequeue(&openNodes);
		if (isTerminal(n))
		{
			List *l = createList();
			while (n->parent != NULL)
			{
				l->push(&l, n->actionToReach);
				n = n->parent;
			}
			while (listSize(l) > 0)
			{
				steps->push(&steps, l->pop(&l));
			}
			freePriorityQueue(openNodes, NULL);
			freeList(l, NULL);
			freeAStarTree(ast);
			return steps;
		}

		expand(n);

		for (long i = 0; i < n->nextStatesLength; i++)
		{
			openNodes->enqueue(&openNodes, (void *)n->nextStates[i], n->nextStates[i]->heuristicScore);
		}
		if (root->height > maxDepth)
		{
			PriorityQueue *toSave = createPriorityQueue(/*orderAscending:*/evaluateIsHeuristic);

			StateNode *nextRoot = (StateNode *)openNodes->dequeue(&openNodes);
			toSave->enqueue(&toSave, (void *)nextRoot, nextRoot->heuristicScore);
			
			while (nextRoot->parent != root)
			{
				nextRoot = nextRoot->parent;
			}
			nextRoot->parent = NULL;
			
			
			while (queueSize(openNodes) > 0)
			{
				StateNode *mayBeSaved = (StateNode *)openNodes->dequeue(&openNodes);
				StateNode *runner = mayBeSaved;
				while (runner != NULL && runner != nextRoot)
				{
					runner = runner->parent;
				}
				if (runner == nextRoot)
				{
					toSave->enqueue(&toSave, (void *)mayBeSaved, mayBeSaved->heuristicScore);
				}
			}
			while (queueSize(toSave) > 0)
			{
				StateNode *saved = toSave->dequeue(&toSave);
				openNodes->enqueue(&openNodes, saved, saved->heuristicScore);
			}
			freePriorityQueue(toSave, NULL);
			for (long i = 0; i < root->nextStatesLength; i++)
			{
				if (root->nextStates[i] != nextRoot)
				{
					freeStateNodeRecursively(root->nextStates[i]);
				}
			}
			freeStateNode(root);
			root = nextRoot;
			ast->tree->root = nextRoot;
			steps->push(&steps, nextRoot->actionToReach);
		}
	}

	return NULL;
}

void freeAStarTree(AStarTree *ast)
{
	freeStateTree(ast->tree);
	free(ast);
}

AStarTree *createAStarTree(
	void *initialState,
	long maxDepth,
	void (*freeAction)(void *action),
	void (*freeState)(void *state),
	long (*evaluate)(void *state))
{
	AStarTree *ast = (AStarTree *)malloc(sizeof(AStarTree));
	ast->tree = createStateTree(freeAction, freeState, evaluate);
	ast->tree->root->state = initialState;
	ast->tree->root->heuristicScore = evaluate(initialState);
	ast->maxDepth = maxDepth;
	return ast;
}
