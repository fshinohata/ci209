#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "collections.h"
#include "stateTree.h"
#include "floodIt.h"

#define MINIMUM_ALLOCATION 32
#define max(a, b) ((a) > (b) ? (a) : (b))
#define useResult(a) if (a) {}

/*
 * Interface for AI algorithms
 */

bool isTerminal(StateNode *sn)
{
    FloodItGraph *t = (FloodItGraph *)sn->state;
    return isGameOver(t);
}

void expand(StateNode *sn)
{
    FloodItGraph *t = (FloodItGraph *)sn->state;
    for (long i = 1; i <= t->numberOfColors; i++)
    {
        if (i != t->lastColorUsed)
        {
            FloodItGraph *t2 = copyFloodItGraph(t);
            long painted = paint(t2, i);

            if (painted > 0)
            {
                addNextState(sn, (void *)t2, (void *)i);
            }
            else
            {
                freeFloodItGraph(t2);
            }
        }
    }
}

void freeState(void *state)
{
    freeFloodItGraph((FloodItGraph *)state);
}

void freeAction(void *action) { if(action){} }

long score(void *state)
{
    FloodItGraph *g = (FloodItGraph *)state;

    if (isGameOver(g))
    {
    	return ~(1L << 63);
    }
    return (g->root->numberOfElements + g->root->neighborhoodLength) - (g->numberOfColors * g->steps);
}

long heuristic(void *state)
{
    FloodItGraph *g = (FloodItGraph *)state;
    return g->steps + farthestNodeDistance(g);
}

/*
 * Structures
 */
void freeFloodItBoard(FloodItBoard *b)
{
    free(b->board);
    free(b);
}

FloodItBoard *createFloodItBoard(long n, long m, long numberOfColors)
{
    FloodItBoard *b = (FloodItBoard *)malloc(sizeof(FloodItBoard));
    b->board = (FloodItBoardElement *)malloc(n * m * sizeof(FloodItBoardElement));
    b->lines = n;
    b->columns = m;
    b->numberOfColors = numberOfColors;
    for (long i = 0; i < n; i++)
    {
        for (long j = 0; j < m; j++)
        {
            floodItBoardElementColor(b,i,j) = -1;
            floodItBoardElementIsProcessed(b,i,j) = false;
        }
    }
    return b;
}

void addNeighbor(FloodItGraphNode *n, FloodItGraphNode *neighbor)
{
    n->neighborhood[n->neighborhoodLength] = neighbor;
    (n->neighborhoodLength)++;
    if (n->neighborhoodLength == n->neighborhoodAllocated)
    {
        n->neighborhoodAllocated <<= 1;
        n->neighborhood = (FloodItGraphNode **)realloc(n->neighborhood, n->neighborhoodAllocated * sizeof(FloodItGraphNode *));
    }
}

void removeNeighbor(FloodItGraphNode *n, FloodItGraphNode *possibleNeighbor)
{
    for (long i = 0; i < n->neighborhoodLength; i++)
    {
        if (n->neighborhood[i] == possibleNeighbor)
        {
            for (long j = i; j < n->neighborhoodLength - 1; j++)
            {
                n->neighborhood[j] = n->neighborhood[j + 1];
            }
            (n->neighborhoodLength)--;
            return;
        }
    }
}

bool hasNeighbor(FloodItGraphNode *n, FloodItGraphNode *possibleNeighbor)
{
    for (long i = 0; i < n->neighborhoodLength; i++)
    {
        if (n->neighborhood[i] == possibleNeighbor)
        {
            return true;
        }
    }
    return false;
}


void consumeNeighbor(FloodItGraphNode *consumer, FloodItGraphNode *consumed)
{
    /* remove consumer from consumed neighborhood */
    removeNeighbor(consumed, consumer);

    /* remove consumed from consumer neighborhood */
    removeNeighbor(consumer, consumed);

    /* remove consumed from all neighbors and swap it with consumer */
	for (long i = 0; i < consumed->neighborhoodLength; i++)
    {
        FloodItGraphNode *neighbor = consumed->neighborhood[i];
        removeNeighbor(neighbor, consumed);
        if (!hasNeighbor(neighbor, consumer))
        {
            addNeighbor(neighbor, consumer);
        }
        if (!hasNeighbor(consumer, neighbor))
        {
            addNeighbor(consumer, neighbor);
        }
    }

    consumer->numberOfElements += consumed->numberOfElements;
    freeFloodItGraphNode(consumed);
}

FloodItGraphNode *copyFloodItGraphNode(FloodItGraphNode *n)
{
    FloodItGraphNode *m = createFloodItGraphNode();
    m->color = n->color;
    m->numberOfElements = n->numberOfElements;
    m->index = n->index;
    m->distanceFromRoot = n->distanceFromRoot;
    m->neighborhoodLength = m->neighborhoodAllocated = n->neighborhoodLength;
    m->neighborhood = (FloodItGraphNode **)realloc(m->neighborhood, m->neighborhoodAllocated * sizeof(FloodItGraphNode *));
    return m;
}

void freeFloodItGraphNode(FloodItGraphNode *n)
{
    free(n->neighborhood);
    free(n);
}

FloodItGraphNode *createFloodItGraphNode()
{
    FloodItGraphNode *n = (FloodItGraphNode *)malloc(sizeof(FloodItGraphNode));
    n->neighborhood = (FloodItGraphNode **)malloc(MINIMUM_ALLOCATION * sizeof(FloodItGraphNode *));
    n->neighborhoodLength = 0;
    n->neighborhoodAllocated = MINIMUM_ALLOCATION;
    n->color = -1;
    n->numberOfElements = 0;
    n->distanceFromRoot = 0;
    n->processed = false;
    n->index = -1;
    return n;
}

FloodItGraph *copyFloodItGraph(FloodItGraph *g)
{
    FloodItGraph *h = createFloodItGraph();
    freeFloodItGraphNode(h->root);
    h->numberOfColors = g->numberOfColors;
    h->farthestNodeDistance = g->farthestNodeDistance;
    h->steps = g->steps;
    h->lastColorUsed = g->lastColorUsed;
    /* copy nodes to h without setting neighborhood */
    for (long i = 0; i < arraySize(g->nodes); i++)
    {
        FloodItGraphNode *toCopy = (FloodItGraphNode *)g->nodes->get(g->nodes, i);
        h->nodes->push(&h->nodes, (void *)copyFloodItGraphNode(toCopy));
    }
    /* set neighborhood */
    for (long i = 0; i < arraySize(g->nodes); i++)
    {
        FloodItGraphNode *toCopy = (FloodItGraphNode *)g->nodes->get(g->nodes, i);
        FloodItGraphNode *copy = (FloodItGraphNode *)h->nodes->get(h->nodes, i);
        for (long j = 0; j < toCopy->neighborhoodLength; j++)
        {
            copy->neighborhood[j] = h->nodes->get(h->nodes, toCopy->neighborhood[j]->index);
        }
    }
    h->root = h->nodes->get(h->nodes, g->root->index);
    return h;
}

void removeFloodItGraphNode(FloodItGraph *g, long index)
{
    for (long i = index + 1; i < arraySize(g->nodes); i++)
    {
        FloodItGraphNode *n = g->nodes->get(g->nodes, i);
        (n->index)--;
    }
    g->nodes->removeAt(&g->nodes, index);
}

long calculateDjikstraDistance(FloodItGraphNode *root)
{
    long maximumDistance = 0;
    Queue *toVisit = createQueue();

    toVisit->enqueue(&toVisit, (void *)root);
    while (queueSize(toVisit) > 0)
    {
        FloodItGraphNode *n = (FloodItGraphNode *)toVisit->dequeue(&toVisit);
        for (long i = 0; i < n->neighborhoodLength; i++)
        {
            if (!n->neighborhood[i]->processed)
            {
                n->neighborhood[i]->distanceFromRoot = n->distanceFromRoot + 1;
                n->neighborhood[i]->processed = true;
                toVisit->enqueue(&toVisit, (void *)n->neighborhood[i]);
                maximumDistance = max(maximumDistance, n->neighborhood[i]->distanceFromRoot);
            }
        }
    }
    freeQueue(toVisit, NULL);
    return maximumDistance;
}

long djikstraDistance(FloodItGraph *g)
{
    for (long i = 0; i < arraySize(g->nodes); i++)
    {
        FloodItGraphNode *n = (FloodItGraphNode *)g->nodes->get(g->nodes, i);
        n->processed = false;
        n->distanceFromRoot = 0;
    }
    g->root->processed = true;
    return calculateDjikstraDistance(g->root);
}

long farthestNodeDistance(FloodItGraph *g)
{
    return djikstraDistance(g);
}

bool isGameOver(FloodItGraph *t)
{
	return t->root->neighborhoodLength == 0;
}

long paint(FloodItGraph *g, long color)
{
	FloodItGraphNode *root = g->root;
	long colorsPainted = 0;
    
	for (long i = 0; i < root->neighborhoodLength; i++)
	{
		if (root->neighborhood[i]->color == color)
		{
			colorsPainted += root->neighborhood[i]->numberOfElements;
            removeFloodItGraphNode(g, root->neighborhood[i]->index);
            consumeNeighbor(root, root->neighborhood[i]);
            i--;
		}
	}
	root->color = color;
    (g->steps)++;
    g->lastColorUsed = color;
	return colorsPainted;
}

void freeFloodItGraph(FloodItGraph *g)
{
    while (arraySize(g->nodes) > 0)
    {
        freeFloodItGraphNode((FloodItGraphNode *)g->nodes->pop(&g->nodes));
    }
    freeArray(g->nodes);
    free(g);
}

FloodItGraph *createFloodItGraph()
{
    FloodItGraph *t = (FloodItGraph *)malloc(sizeof(FloodItGraph));
    t->numberOfColors = -1;
    t->farthestNodeDistance = 0;
    t->nodes = createArray();
    t->root = createFloodItGraphNode();
    t->root->color = -1;
    t->steps = 0;
    t->lastColorUsed = -1;
    t->root->numberOfElements = -1;
    return t;
}

FloodItGraph *generateFloodItGraph(FloodItBoard *b)
{
    FloodItGraph *t = createFloodItGraph();
    t->numberOfColors = b->numberOfColors;

    FloodItGraphNode **nodeBoard = (FloodItGraphNode **)malloc(b->lines * b->columns * sizeof(FloodItGraphNode *));

    for (long i = 0; i < b->lines; i++)
    {
        for (long j = 0; j < b->columns; j++)
        {
            FloodItGraphNode *n = createFloodItGraphNode();
            n->color = floodItBoardElementColor(b, i, j);
            n->numberOfElements = 1;

            nodeBoard[(i * b->lines) + j] = n;
        }
    }

    freeFloodItGraphNode(t->root);
    t->root = nodeBoard[0];
    t->lastColorUsed = t->root->color;

    processNodeBoard(t, nodeBoard, b->lines, b->columns);
    free(nodeBoard);

    return t;
}

void reduceGraphFrom(FloodItGraph *g, FloodItGraphNode *n)
{
    if (n->processed)
    {
        return;
    }
    n->processed = true;
    for (long i = 0; i < n->neighborhoodLength; i++)
    {
        if (n->neighborhood[i]->color == n->color)
        {
            consumeNeighbor(n, n->neighborhood[i]);
            i--;
        }
    }
    n->index = arraySize(g->nodes);
    g->nodes->push(&g->nodes, (void *)n);
    for (long i = 0; i < n->neighborhoodLength; i++)
    {
        reduceGraphFrom(g, n->neighborhood[i]);
    }
}

void processNodeBoard(FloodItGraph *g, FloodItGraphNode **nb, long n, long m)
{
    for (long i = 0; i < n; i++)
    {
        for (long j = 0; j < m; j++)
        {
            Coordinate neighbors[8] = {
                { .i = i - 1, .j = j - 1 },
                { .i = i - 1, .j = j     },
                { .i = i - 1, .j = j + 1 },
                { .i = i    , .j = j - 1 },
                { .i = i    , .j = j + 1 },
                { .i = i + 1, .j = j - 1 },
                { .i = i + 1, .j = j     },
                { .i = i + 1, .j = j + 1 },
            };

            for (long k = 0; k < 8; k++)
            {
                Coordinate c = neighbors[k];
                if (c.i >= 0 && c.j >= 0 && c.i < n && c.j < m)
                {
                    addNeighbor(nb[(i * n) + j], nb[(c.i * n) + c.j]);
                }
            }
        }
    }
    reduceGraphFrom(g, nb[0]);
}

void freeCoordinate(Coordinate *c)
{
	free(c);
}

Coordinate *createCoordinate(long i, long j)
{
    Coordinate *c = (Coordinate *)malloc(sizeof(Coordinate));
    c->i = i;
    c->j = j;
    return c;
}

void readBoard(FloodItBoard *b, FILE *in)
{
    for (long i = 0; i < b->lines; i++)
    {
        for (long j = 0; j < b->columns; j++)
        {
            useResult(fscanf(in, "%ld", &floodItBoardElementColor(b, i, j)));
        }
    }
}

void readBoardFromStdin(FloodItBoard *b)
{
    readBoard(b, stdin);
}

void printBoard(FloodItBoard *b)
{
    for (long i = 0; i < b->lines; i++)
    {
        for (long j = 0; j < b->columns; j++)
        {
            printf("%ld ", floodItBoardElementColor(b, i, j));
        }
        printf("\n");
    }
}

void printFloodItGraphNode(FloodItGraphNode *n)
{
    printf("FloodItGraphNode: {\n\taddress: <%p>,\n\tindex: %ld,\n\tcolor: %ld,\n\tnumberOfElements: %ld,\n\t\n\tneighborhoodLength: %ld\n\t",
        n, n->index, n->color, n->numberOfElements, n->neighborhoodLength
    );
    printf("neighborhood: {\n");
    for (long i = 0; i < n->neighborhoodLength; i++)
    {
        printf("\t\t{ address: <%p>, color: %ld, neighborhoodLength: %ld }\n", n->neighborhood[i], n->neighborhood[i]->color, n->neighborhood[i]->neighborhoodLength);
    }
    printf("\t}\n}\n");
    fflush(stdout);
}

void printFloodItGraph(FloodItGraph *t)
{
    if (arraySize(t->nodes) > 0)
    {
    	printf("Graph: {\n\taddress: <%p>,\n\troot: <%p>\n\tnumberOfColors: %ld,\n\tfarthestNodeDistance: %ld,\n\tsteps: %ld,\n\tlastColorUsed: %ld,\n\t",
            t, t->root, t->numberOfColors, t->farthestNodeDistance, t->steps, t->lastColorUsed);
	    FloodItGraphNode *n = (FloodItGraphNode *)t->nodes->get(t->nodes, 0);
	    printf("All nodes: (%ldx%ld", n->color, n->numberOfElements);
	    for (long i = 1; i < arraySize(t->nodes); i++)
	    {
	        n = (FloodItGraphNode *)t->nodes->get(t->nodes, i);
	        printf(", %ldx%ld", n->color, n->numberOfElements);
	    }
	    printf(")\n}\n");
        printf("Nodes:\n");
        for (long i = 0; i < arraySize(t->nodes); i++)
        {
            n = (FloodItGraphNode *)t->nodes->get(t->nodes, i);
            printFloodItGraphNode(n);
        }
    }
    else
    {
    	printf("Graph has no nodes!\n");
    }
    fflush(stdout);
}
