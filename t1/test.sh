#!/bin/bash

# declare -A SAMPLES;
SAMPLES=( "5 5 10" "8 8 10" "9 9 10" "10 10 10" "11 11 10" "12 12 10" "13 13 10" "14 14 10" "20 20 10" "50 50 10" "100 100 10" );
BIN="./prof_bin";

make -B

echo $SAMPLES;
for i in "${SAMPLES[@]}"; do 
	for j in $(seq 1 10); do
		printf "(%s): " "$i $j";
		$BIN/geramapa $i $j | $BIN/floodit_h2 | grep -om 1 -P "[0-9]+" | xargs printf "%s "; 
		$BIN/geramapa $i $j | $BIN/floodit_h6 | grep -om 1 -P "[0-9]+" | xargs printf "%s "; 
		$BIN/geramapa $i $j | ./floodIt | grep -om 1 -P "[0-9]+" | xargs printf "%s "; 
		echo;
	done;
done;