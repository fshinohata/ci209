#ifndef __A_STAR__
#define __A_STAR__

#include <stdbool.h>
#include "collections.h"
#include "stateTree.h"

typedef struct AStarTree AStarTree;

struct AStarTree
{
	StateTree *tree;
	long maxDepth;
};

/* algorithm */
List *aStar(
	void *initialState,
	long maxDepth,
	void (*freeAction)(void *action),
	void (*freeState)(void *state),
	long (*evaluate)(void *state),
	void (*expand)(StateNode *sn),
	bool (*isTerminal)(StateNode *sn),
	bool evaluateIsHeuristic);

/* maintenance operations */
void freeAStarTree(AStarTree *ast);
AStarTree *createAStarTree(
	void *initialState,
	long maxDepth,
	void (*freeAction)(void *action),
	void (*freeState)(void *state),
	long (*evaluate)(void *state));

/* debug operations */
void printAStarMemoryUsage();

#endif
