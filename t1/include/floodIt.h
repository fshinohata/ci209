#ifndef __FLOOD_IT__
#define __FLOOD_IT__

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "collections.h"

typedef struct FloodItBoard FloodItBoard;
typedef struct FloodItBoardElement FloodItBoardElement;
typedef struct FloodItGraph FloodItGraph;
typedef struct FloodItGraphNode FloodItGraphNode;
typedef struct Coordinate Coordinate;

struct FloodItBoardElement
{
	long color;
	bool isProcessed;
};

struct FloodItBoard
{
    FloodItBoardElement *board;
    long lines;
    long columns;
    long numberOfColors;
};

struct FloodItGraphNode
{
	FloodItGraphNode **neighborhood;
	long neighborhoodLength;
	long neighborhoodAllocated;
	long color;
	long numberOfElements;
	long distanceFromRoot; /* must be updated when root changes */
	long index; /* index in the nodes array of FloodItGraph */
	bool processed; /* used for graph processing */
};

struct FloodItGraph
{
	FloodItGraphNode *root;
    Array *nodes;
	long numberOfColors;
    long farthestNodeDistance;
	long steps;
	long lastColorUsed;
};

struct Coordinate
{
	long i;
	long j;
};

#define floodItBoardElement(b, i, j) (b->board[(i) * b->lines + (j)])
#define floodItBoardElementColor(b, i, j) (floodItBoardElement(b, i, j).color)
#define floodItBoardElementIsProcessed(b, i, j) (floodItBoardElement(b, i, j).isProcessed)

/* interface for AI algorithms */
bool isTerminal(StateNode *sn);
void expand(StateNode *sn);
void freeState(void *state);
void freeAction(void *action);
long score(void *state);
long heuristic(void *state);

/* maintenance operations */
void freeFloodItBoard(FloodItBoard *b);
FloodItBoard *createFloodItBoard(long n, long m, long numberOfColors);

FloodItGraphNode *copyFloodItGraphNode(FloodItGraphNode *n);
void consumeNeighbor(FloodItGraphNode *consumer, FloodItGraphNode *consumed);
void addNeighbor(FloodItGraphNode *n, FloodItGraphNode *neighbor);
void freeFloodItGraphNode(FloodItGraphNode *n);
FloodItGraphNode *createFloodItGraphNode();

FloodItGraph *copyFloodItGraph(FloodItGraph *t);
long farthestNodeDistance(FloodItGraph *t);
bool isGameOver(FloodItGraph *t);
long paint(FloodItGraph *t, long color);
void freeFloodItGraph(FloodItGraph *t);
FloodItGraph *createFloodItGraph();
FloodItGraph *generateFloodItGraph(FloodItBoard *b);

void freeCoordinate(Coordinate *c);
Coordinate *createCoordinate(long i, long j);
void processNodeBoard(FloodItGraph *g, FloodItGraphNode **nb, long n, long m);

/* read/write operations */
void readBoard(FloodItBoard *b, FILE *);
void readBoardFromStdin(FloodItBoard *b);

/* debug operations */
void printFloodItMemoryUsage();
void printBoard(FloodItBoard *b);
void printFloodItGraphNode(FloodItGraphNode *n);
void printFloodItGraph(FloodItGraph *t);

/* game operations */
long paint(FloodItGraph *t, long color);
bool isGameOver(FloodItGraph *t);

#endif
