#ifndef __COLLECTIONS__
#define __COLLECTIONS__

#include <stdbool.h>

typedef struct ListElement ListElement;
typedef struct List List;
typedef struct QueueElement QueueElement;
typedef struct Queue Queue;
typedef struct PriorityQueueElement PriorityQueueElement;
typedef struct PriorityQueue PriorityQueue;
typedef struct ArrayElement ArrayElement;
typedef struct Array Array;

struct ListElement
{
    ListElement *previous;
    ListElement *next;
    void *data;
};

struct List
{
    ListElement *head;
    ListElement *tail;
    long size;
    ListElement *(*push)(List **lp, void *data);
    void *(*pop)(List **lp);
    ListElement *(*unshift)(List **lp, void *data);
    void *(*shift)(List **lp);
    void *(*remove)(List **lp, ListElement *le);
};

struct QueueElement
{
	QueueElement *previous;
	QueueElement *next;
	void *data;
};

struct Queue
{
	QueueElement *head;
	QueueElement *tail;
	QueueElement *(*enqueue)(Queue **qp, void *data);
	void *(*dequeue)(Queue **qp);
	void *(*remove)(Queue **qp, QueueElement *qe);
	long size;
};

struct PriorityQueueElement
{
	PriorityQueueElement *previous;
	PriorityQueueElement *next;
	long priority;
	void *data;
};

struct PriorityQueue
{
	PriorityQueueElement *head;
	PriorityQueueElement *tail;
	PriorityQueueElement *(*enqueue)(PriorityQueue **qp, void *data, long priority);
	void *(*dequeue)(PriorityQueue **qp);
	void *(*remove)(PriorityQueue **qp, PriorityQueueElement *qe);
	bool (*order)(PriorityQueueElement *a, PriorityQueueElement *b);
	long size;
};

struct ArrayElement
{
    void *data;
};

struct Array
{
    ArrayElement **array;
    long arrayLength;
    long arrayAllocated;
    void *(*get)(Array *a, long index);
    ArrayElement *(*push)(Array **a, void *data);
    void *(*pop)(Array **a);
    void *(*removeAt)(Array **a, long index);
};

/* list operations */
#define listSize(l) (l->size)
void freeList(List *l, void (*freeData)(void *data));
List *createList();

/* queue operations */
#define queueSize(q) (q->size)
void freeQueue(Queue *q, void (*freeData)(void *data));
Queue *createQueue();

/* priority queue operations */
void freePriorityQueue(PriorityQueue *pq, void (*freeData)(void *data));
PriorityQueue *createPriorityQueue(bool orderAscending);

/* array operations */
#define arraySize(a) (a->arrayLength)
void freeArray(Array *a);
Array *createArray();

/* debug operations */
void printCollectionsMemoryUsage();
void printQueue(Queue *q, const char *(*printQueueElementData)(void *data));
void printPriorityQueue(PriorityQueue *q);

#endif
