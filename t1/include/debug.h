#ifndef __DEBUG__
#define __DEBUG__

#define D1(commands)
#define PD1(args...)
#define D2(commands)
#define PD2(args...)
#define D3(commands)
#define PD3(args...)
#define D4(commands)
#define PD4(args...)
#define D5(commands)
#define PD5(args...)

#ifdef DEBUG_LEVEL
	#if DEBUG_LEVEL > 0
		#undef D1
		#undef PD1
		#define D1(commands) commands
		#define PD1(args...) printf(args);
	#endif
	#if DEBUG_LEVEL > 1
		#undef D2
		#undef PD2
		#define D2(commands) commands
		#define PD2(args...) printf(args);
	#endif
	#if DEBUG_LEVEL > 2
		#undef D3
		#undef PD3
		#define D3(commands) commands
		#define PD3(args...) printf(args);
	#endif
	#if DEBUG_LEVEL > 3
		#undef D4
		#undef PD4
		#define D4(commands) commands
		#define PD4(args...) printf(args);
	#endif
	#if DEBUG_LEVEL > 4
		#undef D5
		#undef PD5
		#define D5(commands) commands
		#define PD5(args...) printf(args);
	#endif
#endif

#endif