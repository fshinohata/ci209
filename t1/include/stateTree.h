#ifndef __STATE_TREE__
#define __STATE_TREE__

typedef struct StateNode StateNode;
typedef struct StateTree StateTree;

struct StateNode
{
    StateNode **nextStates;
    long nextStatesLength;
    long nextStatesAllocated;

    StateNode *parent;
    StateTree *tree;

    void *actionToReach;

    long heuristicScore;
    long height;

    void *state;
};

struct StateTree
{
	StateNode *root;
	void (*freeAction)(void *action);
	void (*freeState)(void *state);
	long (*heuristic)(void *state);
};

long treeDepth(StateTree *tree);
void addNextState(StateNode *parent, void *state, void *actionApplied);
void freeStateNodeRecursively(StateNode *sn);
void freeStateNode(StateNode *sn);
StateNode *createStateNode(StateTree *tree, StateNode *parent);
void freeWholeStateTree(StateTree *st);
void freeStateTree(StateTree *st);
StateTree *createStateTree(
	void (*freeAction)(void *action), 
	void (*freeState)(void *state),
	long (*heuristic)(void *state));

/* debug operations */
void printStateNode(StateNode *sn);
void printStateTree(StateTree *st);

#endif